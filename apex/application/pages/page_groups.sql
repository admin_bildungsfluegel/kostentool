prompt --application/pages/page_groups
begin
--   Manifest
--     PAGE GROUPS: 100
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(9170372593020643)
,p_group_name=>'Administration'
);
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(9503893255673984)
,p_group_name=>'Arbeitszeiten'
);
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(9503729496673272)
,p_group_name=>'Buchhaltung'
);
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(9503962676674731)
,p_group_name=>'Projektabrechnung'
);
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(9267078444446972)
,p_group_name=>'Stammdaten'
);
wwv_flow_api.component_end;
end;
/
