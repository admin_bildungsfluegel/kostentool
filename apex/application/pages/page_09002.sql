prompt --application/pages/page_09002
begin
--   Manifest
--     PAGE: 09002
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_page(
 p_id=>9002
,p_user_interface_id=>wwv_flow_api.id(9167296431020580)
,p_name=>'Benutzer bearbeiten'
,p_alias=>'BENUTZER-BEARBEITEN'
,p_step_title=>'Benutzer bearbeiten'
,p_autocomplete_on_off=>'OFF'
,p_group_id=>wwv_flow_api.id(9170372593020643)
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'BILDUNGSFLUEGEL'
,p_last_upd_yyyymmddhh24miss=>'20220126180025'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9218519845245957)
,p_plug_name=>'Benutzer bearbeiten'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'TABLE'
,p_query_table=>'T_USER'
,p_include_rowid_column=>false
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_plug_source_type=>'NATIVE_FORM'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9182981488142902)
,p_plug_name=>'Roles'
,p_parent_plug_id=>wwv_flow_api.id(9218519845245957)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader js-removeLandmark:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>20
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P9002_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9183294783142905)
,p_plug_name=>'User'
,p_parent_plug_id=>wwv_flow_api.id(9218519845245957)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader js-removeLandmark:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9231008590246013)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useBreadcrumbTitle'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9087085971020399)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(9020077304020263)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(9144175122020489)
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9224269242245975)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(9218519845245957)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P9002_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9223086284245972)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(9218519845245957)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:9001:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9224632904245975)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(9218519845245957)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P9002_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9223887008245975)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(9218519845245957)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P9002_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(9224980110245976)
,p_branch_action=>'f?p=&APP_ID.:9001:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9183039291142903)
,p_name=>'P9002_ID_ROLES'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(9182981488142902)
,p_prompt=>'Nutzerrollen'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ID_ROLE',
'from T_USER_ROLE',
'where ID_USER = :P9002_ID'))
,p_source_type=>'QUERY_COLON'
,p_display_as=>'NATIVE_SHUTTLE'
,p_named_lov=>'ROLE_LOV'
,p_cHeight=>5
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'ALL'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9218870183245958)
,p_name=>'P9002_ID'
,p_source_data_type=>'NUMBER'
,p_is_primary_key=>true
,p_is_query_only=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(9183294783142905)
,p_item_source_plug_id=>wwv_flow_api.id(9218519845245957)
,p_source=>'ID'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9219674120245969)
,p_name=>'P9002_MAIL'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(9183294783142905)
,p_item_source_plug_id=>wwv_flow_api.id(9218519845245957)
,p_prompt=>'Mail'
,p_source=>'MAIL'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>500
,p_begin_on_new_line=>'N'
,p_read_only_when=>'P9002_ID'
,p_read_only_when_type=>'ITEM_IS_NOT_NULL'
,p_field_template=>wwv_flow_api.id(9141544202020477)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9220064616245970)
,p_name=>'P9002_FIRSTNAME'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(9183294783142905)
,p_item_source_plug_id=>wwv_flow_api.id(9218519845245957)
,p_prompt=>'Firstname'
,p_source=>'FIRSTNAME'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>100
,p_field_template=>wwv_flow_api.id(9141544202020477)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9220802444245970)
,p_name=>'P9002_LASTNAME'
,p_source_data_type=>'VARCHAR2'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(9183294783142905)
,p_item_source_plug_id=>wwv_flow_api.id(9218519845245957)
,p_prompt=>'Lastname'
,p_source=>'LASTNAME'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>100
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9225890539245978)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(9218519845245957)
,p_process_type=>'NATIVE_FORM_DML'
,p_process_name=>'Process form Benutzer bearbeiten'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(9224269242245975)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9025181844829304)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Add new user'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'  PKG_USER_UTIL.ADD_USER(P_MAIL => :P9002_MAIL, P_FIRSTNAME => initcap(:P9002_FIRSTNAME), P_LASTNAME => initcap(:P9002_LASTNAME), P_PASSWORD => ''Bildungsfluegel123!'');',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(9224632904245975)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9183128928142904)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Process P9002_ID_ROLES'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    --delete old data',
'    delete from T_USER_ROLE',
'    where ID_USER = :P9002_ID;',
'',
'    --save new data',
'    merge into T_USER_ROLE UR using (',
'        select COLUMN_VALUE ID_ROLE',
'        from APEX_STRING.SPLIT(:P9002_ID_ROLES, '':'')',
'    ) X ',
'    on (UR.ID_USER = :P9002_ID and UR.ID_ROLE = X.ID_ROLE)',
'    when not matched then',
'    insert (ID_USER, ID_ROLE)',
'    values (:P9002_ID, X.ID_ROLE);',
'    commit;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9225412859245978)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_region_id=>wwv_flow_api.id(9218519845245957)
,p_process_type=>'NATIVE_FORM_INIT'
,p_process_name=>'Initialize form Benutzer bearbeiten'
);
wwv_flow_api.component_end;
end;
/
