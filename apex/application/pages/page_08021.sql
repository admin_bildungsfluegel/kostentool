prompt --application/pages/page_08021
begin
--   Manifest
--     PAGE: 08021
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_page(
 p_id=>8021
,p_user_interface_id=>wwv_flow_api.id(9167296431020580)
,p_name=>'Projekt bearbeiten'
,p_alias=>'PROJEKT-BEARBEITEN'
,p_step_title=>'Projekt bearbeiten'
,p_autocomplete_on_off=>'OFF'
,p_group_id=>wwv_flow_api.id(9267078444446972)
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'BILDUNGSFLUEGEL'
,p_last_upd_yyyymmddhh24miss=>'20220126180216'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9278606354516364)
,p_plug_name=>'Projekt bearbeiten'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader js-removeLandmark:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'TABLE'
,p_query_table=>'T_PROJECT'
,p_include_rowid_column=>false
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_plug_source_type=>'NATIVE_FORM'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9183908357142912)
,p_plug_name=>'Projekt'
,p_parent_plug_id=>wwv_flow_api.id(9278606354516364)
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9468368321699704)
,p_plug_name=>'Tabs'
,p_parent_plug_id=>wwv_flow_api.id(9278606354516364)
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9046626171020367)
,p_plug_display_sequence=>30
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_plug_source_type=>'NATIVE_DISPLAY_SELECTOR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P8021_ID'
,p_attribute_01=>'STANDARD'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9184013630142913)
,p_plug_name=>'Mitarbeiter'
,p_parent_plug_id=>wwv_flow_api.id(9468368321699704)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader js-removeLandmark:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_new_grid_column=>false
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P8021_ID'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9468452929699705)
,p_plug_name=>'Projektleitung'
,p_parent_plug_id=>wwv_flow_api.id(9468368321699704)
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader js-removeLandmark:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(9077633993020394)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_required_role=>wwv_flow_api.id(9473437486756914)
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(9289816994516387)
,p_plug_name=>'Breadcrumb'
,p_region_template_options=>'#DEFAULT#:t-BreadcrumbRegion--useBreadcrumbTitle'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(9087085971020399)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_menu_id=>wwv_flow_api.id(9020077304020263)
,p_plug_source_type=>'NATIVE_BREADCRUMB'
,p_menu_template_id=>wwv_flow_api.id(9144175122020489)
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9283612241516374)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(9278606354516364)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Speichern'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P8021_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9282490526516370)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(9278606354516364)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_image_alt=>'Abbrechen'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:8020:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9284067667516374)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(9278606354516364)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Erstellen'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P8021_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(9283248569516373)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(9278606354516364)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(9142763661020486)
,p_button_image_alt=>unistr('L\00F6schen')
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P8021_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(9024858642829301)
,p_branch_name=>'Go To Page 8021'
,p_branch_action=>'f?p=&APP_ID.:8021:&SESSION.::&DEBUG.:8021:P8021_ID:&P8021_ID.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_when_button_id=>wwv_flow_api.id(9284067667516374)
,p_branch_sequence=>10
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(9284328068516374)
,p_branch_name=>'Go To Page 8020'
,p_branch_action=>'f?p=&APP_ID.:8020:&SESSION.::&DEBUG.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>20
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9071866556585703)
,p_name=>'P8021_IS_ACTIVE'
,p_source_data_type=>'NUMBER'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_item_default=>'1'
,p_prompt=>'Aktiv'
,p_source=>'IS_ACTIVE'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_YES_NO'
,p_field_template=>wwv_flow_api.id(9141544202020477)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9184135325142914)
,p_name=>'P8021_PROJECT_USER'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(9184013630142913)
,p_prompt=>'Mitarbeiter zuordnen'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ID_USER',
'from T_PROJECT_USER',
'where ID_PROJECT = :P8021_ID',
'and IS_MANAGEMENT = 0'))
,p_source_type=>'QUERY_COLON'
,p_display_as=>'NATIVE_SHUTTLE'
,p_named_lov=>'USER_LOV'
,p_cHeight=>5
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'ALL'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9278911675516365)
,p_name=>'P8021_ID'
,p_source_data_type=>'NUMBER'
,p_is_primary_key=>true
,p_is_query_only=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_source=>'ID'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_is_persistent=>'N'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9279358448516367)
,p_name=>'P8021_NAME'
,p_source_data_type=>'VARCHAR2'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_prompt=>'Name'
,p_source=>'NAME'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>60
,p_cMaxlength=>200
,p_field_template=>wwv_flow_api.id(9141544202020477)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9279710918516367)
,p_name=>'P8021_DATE_FROM'
,p_source_data_type=>'DATE'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_prompt=>'Projektstart'
,p_source=>'DATE_FROM'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER_JET'
,p_cSize=>32
,p_cMaxlength=>255
,p_field_template=>wwv_flow_api.id(9141544202020477)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9280183317516368)
,p_name=>'P8021_DATE_TO'
,p_source_data_type=>'DATE'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_prompt=>'Projektende'
,p_source=>'DATE_TO'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_DATE_PICKER_JET'
,p_cSize=>32
,p_cMaxlength=>255
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'N'
,p_attribute_02=>'POPUP'
,p_attribute_03=>'NONE'
,p_attribute_06=>'NONE'
,p_attribute_09=>'N'
,p_attribute_11=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9280583026516368)
,p_name=>'P8021_DESCRIPTION'
,p_source_data_type=>'VARCHAR2'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_prompt=>'Description'
,p_source=>'DESCRIPTION'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>1000
,p_cHeight=>4
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9468514634699706)
,p_name=>'P8021_PROJECT_USER_MANAGEMENT'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(9468452929699705)
,p_prompt=>'Projektleiter zuordnen'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ID_USER',
'from T_PROJECT_USER',
'where ID_PROJECT = :P8021_ID',
'and IS_MANAGEMENT = 1'))
,p_source_type=>'QUERY_COLON'
,p_display_as=>'NATIVE_SHUTTLE'
,p_named_lov=>'USER_LOV'
,p_cHeight=>5
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_security_scheme=>wwv_flow_api.id(9169745366020638)
,p_attribute_01=>'ALL'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(9471853107699739)
,p_name=>'P8021_ID_COLOR'
,p_source_data_type=>'NUMBER'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(9183908357142912)
,p_item_source_plug_id=>wwv_flow_api.id(9278606354516364)
,p_prompt=>'Kalenderfarbe'
,p_source=>'ID_COLOR'
,p_source_type=>'REGION_SOURCE_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_named_lov=>'COLOR_LOV'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_begin_on_new_line=>'N'
,p_field_template=>wwv_flow_api.id(9140275770020475)
,p_item_template_options=>'#DEFAULT#'
,p_is_persistent=>'N'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9285250144516375)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(9278606354516364)
,p_process_type=>'NATIVE_FORM_DML'
,p_process_name=>'Process form Projekt bearbeiten'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Projekt erfolgreich angelegt.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9184224224142915)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Process P8021_PROJECT_USER'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    --delete old data',
'    delete from T_PROJECT_USER',
'    where ID_PROJECT = :P8021_ID',
'    and IS_MANAGEMENT = 0;',
'',
'    --save new data',
'    merge into T_PROJECT_USER PU using ( select column_value ID_USER',
'                                         from APEX_STRING.SPLIT(:P8021_PROJECT_USER, '':'')',
'                                       ) X',
'    on (PU.ID_USER = X.ID_USER and PU.ID_PROJECT = :P8021_ID)                                   ',
'    when not matched then',
'    insert (ID_USER, ID_PROJECT, IS_MANAGEMENT)',
'    values (X.ID_USER, :P8021_ID, 0);',
'',
'    commit;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9468687033699707)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Process P8021_PROJECT_USER_MANAGEMENT'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    --delete old data',
'    delete from T_PROJECT_USER',
'    where ID_PROJECT = :P8021_ID',
'    and IS_MANAGEMENT = 1;',
'',
'    --save new data',
'    merge into T_PROJECT_USER PU using ( select column_value ID_USER',
'                                         from APEX_STRING.SPLIT(:P8021_PROJECT_USER_MANAGEMENT, '':'')',
'                                       ) X',
'    on (PU.ID_USER = X.ID_USER and PU.ID_PROJECT = :P8021_ID)                                   ',
'    when not matched then',
'    insert (ID_USER, ID_PROJECT, IS_MANAGEMENT)',
'    values (X.ID_USER, :P8021_ID, 1);',
'',
'    commit;',
'end;'))
,p_process_clob_language=>'PLSQL'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(9284817908516375)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_region_id=>wwv_flow_api.id(9278606354516364)
,p_process_type=>'NATIVE_FORM_INIT'
,p_process_name=>'Initialize form Projekt bearbeiten'
);
wwv_flow_api.component_end;
end;
/
