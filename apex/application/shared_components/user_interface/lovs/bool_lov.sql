prompt --application/shared_components/user_interface/lovs/bool_lov
begin
--   Manifest
--     BOOL_LOV
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(9070618292581377)
,p_lov_name=>'BOOL_LOV'
,p_lov_query=>'.'||wwv_flow_api.id(9070618292581377)||'.'
,p_location=>'STATIC'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(9070906712581380)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Ja'
,p_lov_return_value=>'1'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(9071365082581381)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Nein'
,p_lov_return_value=>'0'
);
wwv_flow_api.component_end;
end;
/
