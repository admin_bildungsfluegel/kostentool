prompt --application/shared_components/user_interface/lovs/project_active_lov
begin
--   Manifest
--     PROJECT_ACTIVE_LOV
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(9080165443712573)
,p_lov_name=>'PROJECT_ACTIVE_LOV'
,p_source_type=>'TABLE'
,p_location=>'LOCAL'
,p_use_local_sync_table=>false
,p_query_table=>'T_PROJECT'
,p_query_where=>'IS_ACTIVE = 1'
,p_return_column_name=>'ID'
,p_display_column_name=>'NAME'
,p_group_sort_direction=>'ASC'
,p_default_sort_column_name=>'NAME'
,p_default_sort_direction=>'ASC'
);
wwv_flow_api.component_end;
end;
/
