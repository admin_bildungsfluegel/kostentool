prompt --application/shared_components/logic/application_processes/set_g_env
begin
--   Manifest
--     APPLICATION PROCESS: set G_ENV
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_flow_process(
 p_id=>wwv_flow_api.id(9037004847026542)
,p_process_sequence=>1
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'set G_ENV'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select VALUE',
'into :G_ENV',
'from DATABASE_PROPERTIES',
'where ATTRIBUTE = ''ENV'';'))
,p_process_clob_language=>'PLSQL'
);
wwv_flow_api.component_end;
end;
/
