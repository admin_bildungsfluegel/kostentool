prompt --application/shared_components/logic/application_items/g_user_id
begin
--   Manifest
--     APPLICATION ITEM: G_USER_ID
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(9180581876068748)
,p_name=>'G_USER_ID'
,p_protection_level=>'I'
);
wwv_flow_api.component_end;
end;
/
