prompt --application/shared_components/logic/application_settings
begin
--   Manifest
--     APPLICATION SETTINGS: 100
--   Manifest End
wwv_flow_api.component_begin (
 p_version_yyyy_mm_dd=>'2021.04.15'
,p_release=>'21.1.7'
,p_default_workspace_id=>9015224248952666
,p_default_application_id=>100
,p_default_id_offset=>0
,p_default_owner=>'BILDUNGSFLUEGEL'
);
wwv_flow_api.create_app_setting(
 p_id=>wwv_flow_api.id(9031570730970759)
,p_name=>'G_ENV'
,p_value=>'DEV'
,p_is_required=>'Y'
);
wwv_flow_api.component_end;
end;
/
