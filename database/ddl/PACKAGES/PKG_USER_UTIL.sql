--------------------------------------------------------
--  DDL for Package PKG_USER_UTIL
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "BILDUNGSFLUEGEL"."PKG_USER_UTIL" as
  function GET_HASH (
    P_MAIL     in varchar2
  , P_PASSWORD in varchar2
  ) return varchar2;

  procedure ADD_USER (
    P_MAIL      in varchar2
  , P_FIRSTNAME in varchar2
  , P_LASTNAME  in varchar2
  , P_PASSWORD  in varchar2
  );

  procedure CHANGE_PASSWORD (
    P_MAIL         in varchar2
  , P_OLD_PASSWORD in varchar2
  , P_NEW_PASSWORD in varchar2
  );

  procedure SEND_VERIFICATION_LINK (
    P_MAIL              in varchar2
  , P_VERIFICATION_HASH in varchar2
  );

  procedure VALID_USER (
    P_MAIL in varchar2
  , P_PASSWORD in varchar2
  );

  function VALID_USER (
    P_MAIL in varchar2
  , P_PASSWORD in varchar2
  ) return number;

  procedure RESET_PASSWORD (
    P_MAIL in varchar2
  );

  procedure SET_USER_APP_ITEMS;

end;

/
