--------------------------------------------------------
--  DDL for Package PKG_DB_LOGGING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE "BILDUNGSFLUEGEL"."PKG_DB_LOGGING" as
  procedure LOG_AT (
    P_SOURCE      in   varchar2
    , P_CODE        in   varchar2
    , P_MESSAGE     in   varchar2
  );

  procedure LOG_INFO_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  );

  procedure LOG_DEBUG_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  );

  procedure LOG_ERROR_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  );

end PKG_DB_LOGGING;

/
