--------------------------------------------------------
--  DDL for Table DATABASE_PROPERTIES
--------------------------------------------------------

  CREATE TABLE "BILDUNGSFLUEGEL"."DATABASE_PROPERTIES" 
   (	"ATTRIBUTE" VARCHAR2(100 CHAR) COLLATE "USING_NLS_COMP", 
	"VALUE" VARCHAR2(100 CHAR) COLLATE "USING_NLS_COMP"
   )  DEFAULT COLLATION "USING_NLS_COMP" SEGMENT CREATION IMMEDIATE 
  PCTFREE 10 PCTUSED 40 INITRANS 10 MAXTRANS 255 
 NOCOMPRESS LOGGING
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA" ;
