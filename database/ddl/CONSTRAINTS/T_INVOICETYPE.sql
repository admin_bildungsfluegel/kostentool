--------------------------------------------------------
--  Constraints for Table T_INVOICETYPE
--------------------------------------------------------

  ALTER TABLE "BILDUNGSFLUEGEL"."T_INVOICETYPE" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INVOICETYPE" MODIFY ("NAME" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INVOICETYPE" ADD CONSTRAINT "PK_T_EXPENSETYPE" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 20 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA"  ENABLE;
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INVOICETYPE" ADD CONSTRAINT "UQ_T_EXPENSETYPE" UNIQUE ("NAME")
  USING INDEX PCTFREE 10 INITRANS 20 MAXTRANS 255 COMPUTE STATISTICS 
  STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645
  PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1
  BUFFER_POOL DEFAULT FLASH_CACHE DEFAULT CELL_FLASH_CACHE DEFAULT)
  TABLESPACE "DATA"  ENABLE;
