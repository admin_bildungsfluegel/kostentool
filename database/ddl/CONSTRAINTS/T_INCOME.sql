--------------------------------------------------------
--  Constraints for Table T_INCOME
--------------------------------------------------------

  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" MODIFY ("ID" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" MODIFY ("ID_PROJECT" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" MODIFY ("ID_PAYMENTMETHOD" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" MODIFY ("SENDER" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" MODIFY ("AMOUNT" NOT NULL ENABLE);
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" ADD CONSTRAINT "PK_T_INCOME" PRIMARY KEY ("ID")
  USING INDEX PCTFREE 10 INITRANS 20 MAXTRANS 255 
  TABLESPACE "DATA"  ENABLE;
