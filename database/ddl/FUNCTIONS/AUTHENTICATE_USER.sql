--------------------------------------------------------
--  DDL for Function AUTHENTICATE_USER
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE FUNCTION "BILDUNGSFLUEGEL"."AUTHENTICATE_USER" (
  P_USERNAME       in   varchar2
  , P_PASSWORD   in   varchar2
) return boolean is

  V_MAIL              T_USER.MAIL%type;
  V_PASSWORD_HASH     T_USER.PASSWORD_HASH%type;
  V_HASHED_PASSWORD   T_USER.PASSWORD_HASH%type;
  V_COUNT             number;
begin
  pkg_db_logging.log_info_at('AUTHENTICATE_USER', 'Start of user authentication');
  --check if user exists
  select count(ID), MAIL
  into V_COUNT, V_MAIL
  from T_USER
  where upper(USERNAME) = upper(P_USERNAME)
  group by MAIL;
  
  pkg_db_logging.log_info_at('AUTHENTICATE_USER', 'V_COUNT: ' || V_COUNT);
  pkg_db_logging.log_info_at('AUTHENTICATE_USER', 'V_MAIL: ' || V_MAIL);

  if V_COUNT = 0 then
    --mail doesn't exist
    APEX_UTIL.SET_AUTHENTICATION_RESULT(1);
  else
    --user exists
    V_HASHED_PASSWORD := PKG_USER_UTIL.GET_HASH(P_MAIL => V_MAIL, P_PASSWORD => P_PASSWORD);
    select PASSWORD_HASH
    into V_PASSWORD_HASH
    from T_USER
    where upper(MAIL) = upper(V_MAIL);
    
  pkg_db_logging.log_info_at('AUTHENTICATE_USER', 'V_PASSWORD_HASH: ' || V_PASSWORD_HASH);
  pkg_db_logging.log_info_at('AUTHENTICATE_USER', 'V_HASHED_PASSWORD: ' || V_HASHED_PASSWORD);

    if V_PASSWORD_HASH = V_HASHED_PASSWORD then
      --password match
      APEX_UTIL.SET_AUTHENTICATION_RESULT(0);
      return TRUE;
    else
      --invalid mail+password
      APEX_UTIL.SET_AUTHENTICATION_RESULT(4);
      return FALSE;
    end if;

  end if;
    --some error occured

  APEX_UTIL.SET_AUTHENTICATION_RESULT(7);
  return FALSE;
exception
  when others then
    --unknown internal error
    APEX_UTIL.SET_AUTHENTICATION_RESULT(7);
    --save errror to Auth Status
    APEX_UTIL.SET_CUSTOM_AUTH_STATUS(SQLERRM);
    return FALSE;
end AUTHENTICATE_USER;

/
