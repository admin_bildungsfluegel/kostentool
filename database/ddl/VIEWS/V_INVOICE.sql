--------------------------------------------------------
--  DDL for View V_INVOICE
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "BILDUNGSFLUEGEL"."V_INVOICE" ("ID", "ID_PROJECT", "INVOICE_NUMBER", "AMOUNT", "PAYMENT_DATE", "RECIPIENT", "DESCRIPTION", "ID_INVOICETYPE", "ID_USER", "ID_TAX", "ID_PAYMENTMETHOD", "ID_PURPOSE", "PAYMENT_DEADLINE") DEFAULT COLLATION "USING_NLS_COMP"  AS 
  select
  I.ID
  , I.ID_PROJECT
  , APEX_STRING.GET_INITIALS(P.NAME, 4) || '-' ||
      APEX_STRING.GET_INITIALS(IT.NAME, 2) || '-' ||
      TRUNC(PAYMENT_DATE, 'YYYY') || '-' ||
      row_number() OVER (partition by P.ID, IT.ID, trunc(payment_date, 'YYYY') order by P.ID, IT.ID, trunc(payment_date, 'YYYY')) 
    INVOICE_NUMBER
  , I.AMOUNT
  , I.PAYMENT_DATE
  , I.RECIPIENT
  , I.DESCRIPTION
  , I.ID_INVOICETYPE
  , I.ID_USER
  , I.ID_TAX
  , I.ID_PAYMENTMETHOD
  , I.ID_PURPOSE
  , I.PAYMENT_DEADLINE
from
  T_INVOICE     I
  left join T_PROJECT     P on P.ID = I.ID_PROJECT
  left join T_INVOICETYPE IT on IT.ID = I.ID_INVOICETYPE
  where P.IS_ACTIVE = 1
;
