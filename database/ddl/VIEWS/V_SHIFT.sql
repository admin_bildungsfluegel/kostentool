--------------------------------------------------------
--  DDL for View V_SHIFT
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "BILDUNGSFLUEGEL"."V_SHIFT" ("ID", "ID_USER", "ID_PROJECT", "ID_ACTIVITY", "DATE_FROM", "DATE_TO", "DESCRIPTION", "HOURS", "LABEL", "CSS_CLASS") DEFAULT COLLATION "USING_NLS_COMP"  AS 
  select S.ID
         , S.ID_USER
         , S.ID_PROJECT
         , S.ID_ACTIVITY
         , S.DATE_FROM
         , S.DATE_TO
         , S.DESCRIPTION
         , ROUND(TO_NUMBER(S.DATE_TO - S.DATE_FROM) * 24, 2) HOURS
         , P.NAME || ': ' || S.DATE_FROM || ' - ' || S.DATE_TO || ' (' || ROUND(TO_NUMBER(S.DATE_TO - S.DATE_FROM) * 24, 2) || 'h)' LABEL
         , C.CSS_CLASS
  from T_SHIFT S
  left join T_PROJECT P on P.ID = S.ID_PROJECT
  left join T_COLOR C on C.ID = P.ID_COLOR
;
