--------------------------------------------------------
--  DDL for View V_WORKDAYS
--------------------------------------------------------

  CREATE OR REPLACE FORCE EDITIONABLE VIEW "BILDUNGSFLUEGEL"."V_WORKDAYS" ("DAY", "WEEKDAY") DEFAULT COLLATION "USING_NLS_COMP"  AS 
  with DAY_COUNT as ( select rownum RN
                      from ALL_OBJECTS
                      where rownum <= ( TO_DATE('31.12.2022') - TO_DATE('01.01.2022') + 1 )
  )
  select TO_DATE('01.01.2022') + DC.RN - 1 day
         , TO_CHAR(TO_DATE('01.01.2022') + DC.RN - 1, 'DY') weekday
  from DAY_COUNT DC
  where TO_CHAR(TO_DATE('01.01.2022') + DC.RN - 1, 'DY') not in (
    'SAT'
    , 'SUN'
  )
;
