---------------------------
--Changed PACKAGE BODY
--PKG_USER_UTIL
---------------------------
CREATE OR REPLACE PACKAGE BODY "BILDUNGSFLUEGEL"."PKG_USER_UTIL" as

  function GET_VERIFICATION_HASH (
    P_MAIL in varchar2
  ) return varchar2 as
  begin
    return DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(UPPER(P_MAIL)), DBMS_CRYPTO.HASH_SH1);
  end GET_VERIFICATION_HASH;

  function GET_HASH (
    P_MAIL       in   varchar2
    , P_PASSWORD   in   varchar2
  ) return varchar2 as
    L_SALT varchar2(255) := 'HTGF64456YDCFXGVB363ETG6354GCBNF964TRGHBSRGV6341ERSGV6';
  begin
    return DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(UPPER(P_MAIL) || L_SALT || UPPER(P_PASSWORD)), DBMS_CRYPTO.HASH_SH1);
  end GET_HASH;

  procedure ADD_USER (
    P_MAIL        in   varchar2
    , P_FIRSTNAME   in   varchar2
    , P_LASTNAME    in   varchar2
    , P_PASSWORD    in   varchar2
  ) as
    V_ID                  T_USER.ID%type;
    V_USERNAME            T_USER.USERNAME%type;
    V_VERIFICATION_HASH   varchar2(2000 char);
    V_VERIFICATION_LINK   varchar2(2000 char);
  begin
    V_USERNAME := replace(upper(P_FIRSTNAME || P_LASTNAME), ' ', '');
  
    insert into T_USER (
      MAIL
      , USERNAME
      , FIRSTNAME
      , LASTNAME
      , PASSWORD_HASH
    ) values (
      UPPER(P_MAIL)
      , V_USERNAME
      , P_FIRSTNAME
      , P_LASTNAME
      , GET_HASH(P_MAIL, P_PASSWORD)
    ) returning ID into V_ID;

    commit;
    V_VERIFICATION_HASH := GET_VERIFICATION_HASH(P_MAIL);
    update T_USER
    set
      VERIFICATION_HASH = V_VERIFICATION_HASH
    where ID = V_ID;

    commit;

    --send_verification_mail(p_mail, v_verification_hash);
  end ADD_USER;

  procedure CHANGE_PASSWORD (
    P_MAIL           in   varchar2
    , P_OLD_PASSWORD   in   varchar2
    , P_NEW_PASSWORD   in   varchar2
  ) as
    V_ROWID rowid;
  begin
    select rowid
    into V_ROWID
    from T_USER
    where MAIL = UPPER(P_MAIL) and PASSWORD_HASH = GET_HASH(P_MAIL, P_OLD_PASSWORD)
    for update;

    update T_USER
    set
      PASSWORD_HASH = GET_HASH(P_MAIL, P_NEW_PASSWORD)
    where rowid = V_ROWID;

    commit;
  exception
    when NO_DATA_FOUND then
      RAISE_APPLICATION_ERROR(-20000, 'Invalid username/password.');
  end CHANGE_PASSWORD;

  procedure SEND_VERIFICATION_LINK (
    P_MAIL                in   varchar2
    , P_VERIFICATION_HASH   in   varchar2
  ) as
    V_VERIFICATION_LINK varchar2(2000 char);
  begin
    V_VERIFICATION_LINK := APEX_PAGE.GET_URL(P_APPLICATION => 100, P_PAGE => 101010, P_REQUEST => 'VERIFY', P_ITEMS => 'P101010_MAIL,P101010_HASH', P_VALUES => DBMS_CRYPTO.HASH(UTL_RAW.CAST_TO_RAW(UPPER
    (P_MAIL)), DBMS_CRYPTO.HASH_SH1));

                           --TODO: send the mail!
  end SEND_VERIFICATION_LINK;

  procedure VALID_USER (
    P_MAIL       in   varchar2
    , P_PASSWORD   in   varchar2
  ) as
    V_DUMMY varchar2(1);
  begin
    select '1'
    into V_DUMMY
    from T_USER
    where MAIL = UPPER(P_MAIL) and PASSWORD_HASH = GET_HASH(P_MAIL, P_PASSWORD);

  exception
    when NO_DATA_FOUND then
      RAISE_APPLICATION_ERROR(-20000, 'Invalid username/password.');
  end;

  function VALID_USER (
    P_MAIL       in   varchar2
    , P_PASSWORD   in   varchar2
  ) return number as
    V_ID     T_USER.ID%type;
    V_MAIL   T_USER.MAIL%type;
    V_USERNAME T_USER.USERNAME%type;
    V_USER_ROLES varchar2(100 char);
  begin
    VALID_USER(P_MAIL, P_PASSWORD);
    --return the user ID to session state
    select ID
           , MAIL
           , USERNAME
    into
      V_ID
    , V_MAIL
    , V_USERNAME
    from T_USER
    where MAIL = P_MAIL and PASSWORD_HASH = GET_HASH(P_MAIL, P_PASSWORD) and IS_VERIFIED = 1; 

    --select Roles from T_ROLE
    select ':' || listagg(ID_ROLE, ':')
    into V_USER_ROLES
    from T_USER_ROLE
    where ID_USER = V_ID;

    APEX_UTIL.SET_SESSION_STATE(P_NAME => 'G_USER_ID', P_VALUE => V_ID);    
    APEX_UTIL.SET_SESSION_STATE(P_NAME => 'G_USERNAME', P_VALUE => V_USERNAME);
    APEX_UTIL.SET_SESSION_STATE(P_NAME => 'G_MAIL', P_VALUE => V_MAIL);
    APEX_UTIL.SET_SESSION_STATE(P_NAME => 'G_USER_ROLES', P_VALUE => V_USER_ROLES);
    APEX_UTIL.SET_AUTHENTICATION_RESULT(0);
    return 1;
  exception
    when no_data_found then
      APEX_UTIL.SET_AUTHENTICATION_RESULT(1);
      return 0;
    when others then
      APEX_UTIL.SET_AUTHENTICATION_RESULT(4);
      return 0;
  end;

  procedure RESET_PASSWORD (
    P_MAIL in varchar2
  ) as
    V_COUNT        number;
    V_RESET_HASH   varchar2(1000 char);
  begin
    --find user_id
    select count(ID)
    into V_COUNT
    from T_USER
    where MAIL = UPPER(P_MAIL);

    if V_COUNT = 0 then
      --Useraccount with P_MAIL not found
      null;
    else
      V_RESET_HASH := GET_HASH(P_MAIL => P_MAIL, P_PASSWORD => P_MAIL || SYSTIMESTAMP);

      --TODO send email with reset link

      update T_USER
      set
        RESET_HASH = V_RESET_HASH
      where MAIL = P_MAIL;

      commit;
    end if;

  end;

  procedure SET_USER_APP_ITEMS as
    V_ID T_USER.ID%type;
    V_MAIL T_USER.MAIL%type;
    V_USER varchar2(300 char);
    V_USERNAME T_USER.USERNAME%type;
    V_ID_ROLES varchar2(300 char);
  begin
    select ID
           , MAIL
           , FIRSTNAME || ' ' || LASTNAME
           , USERNAME
    into   V_ID
           , V_MAIL
           , V_USER
           , V_USERNAME
    from T_USER
    where USERNAME = v('APP_USER');

    select ':' || listagg(ID_ROLE, ':') || ':'
    into V_ID_ROLES
    from T_USER_ROLE
    where ID_USER = V_ID;

    apex_util.set_session_state(P_NAME => 'G_USER_ID', P_VALUE => V_ID);
    apex_util.set_session_state(P_NAME => 'G_MAIL', P_VALUE => V_MAIL);
    APEX_UTIL.SET_SESSION_STATE(P_NAME => 'G_USERNAME', P_VALUE => V_USERNAME);
    apex_util.set_session_state(P_NAME => 'G_ID_ROLES', P_VALUE => V_ID_ROLES);
  end;

end;
/
