--------------------------------------------------------
--  DDL for Package Body PKG_DB_LOGGING
--------------------------------------------------------

  CREATE OR REPLACE EDITIONABLE PACKAGE BODY "BILDUNGSFLUEGEL"."PKG_DB_LOGGING" as procedure LOG_AT (
  P_SOURCE      in   varchar2
  , P_CODE        in   varchar2
  , P_MESSAGE     in   varchar2
) as begin
    insert into T_DB_LOG (
    SOURCE
    , CODE
    , MESSAGE
    , TIME
    ) values ( 
    P_SOURCE
    , P_CODE
    , P_MESSAGE
    , sysdate); 
    
    commit;
  end;
  procedure LOG_INFO_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  ) as
  begin
    LOG_AT(P_SOURCE => P_SOURCE, P_CODE => 'INFO', P_MESSAGE => P_MESSAGE);
  end;

  procedure LOG_DEBUG_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  ) as
  begin
    LOG_AT(P_SOURCE => P_SOURCE, P_CODE => 'DEBUG', P_MESSAGE => P_MESSAGE);
  end;

  procedure LOG_ERROR_AT (
    P_SOURCE      in   varchar2
    , P_MESSAGE     in   varchar2
  ) as
  begin
    LOG_AT(P_SOURCE => P_SOURCE, P_CODE => 'ERROR', P_MESSAGE => P_MESSAGE);
  end;

end PKG_DB_LOGGING;

/
