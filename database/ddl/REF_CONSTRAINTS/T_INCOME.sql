--------------------------------------------------------
--  Ref Constraints for Table T_INCOME
--------------------------------------------------------

  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" ADD CONSTRAINT "FK_T_INCOME_1" FOREIGN KEY ("ID_PROJECT")
	  REFERENCES "BILDUNGSFLUEGEL"."T_PROJECT" ("ID") ENABLE;
  ALTER TABLE "BILDUNGSFLUEGEL"."T_INCOME" ADD CONSTRAINT "FK_T_INCOME_2" FOREIGN KEY ("ID_PAYMENTMETHOD")
	  REFERENCES "BILDUNGSFLUEGEL"."T_PAYMENTMETHOD" ("ID") ENABLE;
