--------------------------------------------------------
--  Ref Constraints for Table T_USER_ROLE
--------------------------------------------------------

  ALTER TABLE "BILDUNGSFLUEGEL"."T_USER_ROLE" ADD CONSTRAINT "FK_T_USER_ROLE_1" FOREIGN KEY ("ID_USER")
	  REFERENCES "BILDUNGSFLUEGEL"."T_USER" ("ID") ON DELETE CASCADE ENABLE;
